# hello_world.tf

resource "null_resource" "hello_world" {
  triggers = {
    message = "Hello, World!"
  }

  provisioner "local-exec" {
    command = "echo ${self.triggers.message}"
  }
}
